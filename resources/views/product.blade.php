@extends('toko.toko')

@section('konten')
    <!-- Produk css -->
    <div class="container" id="produk">
        <div class="row">
            <!--etalase toko-->
            <div class="col-sm-3 mb-3">
                <div class="card" style="width: 15rem;">
                    <div class="card-header">
                        <b>Etalase Toko</b>
                    </div>
                    <ul class="list-group">
                        <a href="#">
                            <li class="list-group-item">Semua Produk</li>
                        </a>
                        <a href="#">
                            <li class="list-group-item">Diskon</li>
                        </a>
                        <a href="#">
                            <li class="list-group-item">Produk Terjual</li>
                        </a>
                        <a href="#">
                            <li class="list-group-item">New Arrival</li>
                        </a>

                    </ul>
                </div>
            </div>
            <!--akhir etalase toko-->


            <!--card produk-->
            <div class="col justify-content-start mb-3">
                <div class="row">
                    <!--button dropdown-->
                    <div class="col text-end">
                        <div class="dropdown ">
                            <span><b>Urutkan</b></span>
                            <button class="btn btn-default dropdown-toggle ms-3 " type="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                Terbaru
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Paling Sesuai</a></li>
                                <li><a class="dropdown-item" href="#">Terbaru</a></li>
                                <li><a class="dropdown-item" href="#">Harga Tertinggi</a></li>
                                <li><a class="dropdown-item" href="#">Harga Terrendah</a></li>
                                <li><a class="dropdown-item" href="#">Ulasan Terbanyak</a></li>
                                <li><a class="dropdown-item" href="#">Pembelian Terbanyak</a></li>
                                <li><a class="dropdown-item" href="#">Dilihat Terbanyak</a></li>
                                <li><a class="dropdown-item" href="#">Pembaruan Terakhir</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--akhir button dropdown-->

                    <!--card produk-->
                    <span><b>Semua Produk</b></span>
                    @for ($i = 0; $i <= 9; $i++)
                        @foreach ($produk as $p)
                            <div class="col-sm-3 mb-3">
                                <p id="angka">{{ $i }}</p>
                                <div class="card" style="height: 95%; width: 100%;">
                                    <img src="{{ asset('storage/product/' . $p->gambar_product) }}" class="card-img-top"
                                        alt="produk" style="width: 100%; height: 150px; object-fit: cover;" />
                                    <div class="card-body">
                                        <p class="card-text">{{ $p->nama_product }}</p>
                                        <b>
                                            <p class="card-text"><span>Rp. </span>{{ number_format($p->harga_product) }}</p>
                                        </b>
                                    </div>
                                    <i class="bi bi-three-dots text-end me-2"></i>
                                </div>
                            </div>
                        @endforeach
                    @endfor
                    <!--Akhir card produk-->

                    <!--pagination-->
                    <div class="daftar-produk mt-4">
                        <ul class="pagination">
                            <p>Jumlah Produk Per halaman</p>
                            <span class="page-item ms-4 me-2">
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <center>
                                            20
                                        </center>
                                    </a>
                                </li>
                            </span>
                            <span class="page-item me-2">
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <center>
                                            40
                                        </center>
                                    </a>
                                </li>
                            </span>
                            <span class="page-item">
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <center>
                                            80
                                        </center>
                                    </a>
                                </li>
                            </span>
                        </ul>
                        <ul class="pagination  justify-content-end ">
                            <span>
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <i class="bi bi-chevron-right"></i>
                                    </a>
                                </li>
                            </span>
                        </ul>
                    </div>

                    <!--akhir pagination-->
                </div>
            </div>
        </div>
    </div>
@endsection
