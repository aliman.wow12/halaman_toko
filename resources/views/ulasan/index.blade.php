@extends('layout.template')

@section('title')
    Daftar ulasan
@endsection

@section('breadcrumb')
    @parent
    <li class="active">ulasan</li>
@endsection
@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <a href="{{ route('ulasan.create') }}" class="btn btn-md btn-success mb-3">TAMBAH ULASAN</a>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">NO</th>
                                    <th scope="col">Profil User</th>
                                    <th scope="col">username</th>
                                    <th scope="col">Gambar Ulasan</th>
                                    <th scope="col">Ulasan</th>
                                    <th scope="col">AKSI</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php $no = 1; ?>
                                @forelse ($ulasan as $u)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>
                                            <img src="{{ Storage::url('public/ulasan/') . $u->profil_user }}"
                                                class="rounded" style="width: 150px">
                                        <td>{{ $u->username }}</td>
                                        <td class="text-center">
                                            <img src="{{ Storage::url('public/ulasan/') . $u->gambar_ulasan }}"
                                                class="rounded" style="width: 150px">
                                        </td>
                                        <td>{{ $u->ulasan }}</td>
                                        <td class="text-center">
                                            <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                                action="{{ route('ulasan.destroy', $u->id) }}" method="POST">
                                                <a href="{{ route('ulasan.edit', $u->id) }}"
                                                    class="btn btn-sm btn-primary">EDIT</a>
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <div class="alert alert-danger">
                                        Data Post belum Tersedia.
                                    </div>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $ulasan->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        //message with toastr
        @if (session()->has('success'))

            toastr.success('{{ session('success') }}', 'BERHASIL!');
        @elseif (session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!');
        @endif
    </script>
@endsection
