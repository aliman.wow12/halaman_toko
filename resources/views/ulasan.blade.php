@extends('toko.toko')

@section('konten')
    <div class="container" id="ulasan">
        <div class="row">
            <div class="col-sm-3 mb-3">
                <i class="bi bi-star-fill text-warning ms-4" style="font-size: 30px"></i>
                <span class="rating">4.8</span>
                <span class="rate">/ 5.0</span>
                <p class="puas ms-4">98% pembeli merasa puas <i class="bi bi-info-circle"></i></p>
                <p id="rate" class="ms-4">40,4rb rating <i class="bi bi-dot"></i> 2rb ulasan</p>
            </div>

            <div class="col justify-content-start-mb-3">
                <div class="row">
                    <div class="col-sm">
                        <!--ulasan-->
                        <span class="ulasan text-uppercase"><b>Semua Pilihan</b></span>
                        <p class="ulasan-title">Menampilkan 1 dari n ulasan</p>
                        @foreach ($ulasan as $u)
                            <div class="col-sm">
                                <i class="bi bi-star-fill text-warning"></i>
                                <i class="bi bi-star-fill text-warning"></i>
                                <i class="bi bi-star-fill text-warning"></i>
                                <i class="bi bi-star-fill text-warning"></i>
                                <i class="bi bi-star-fill text-warning"></i>
                            </div>
                            <div class="col text-end">
                                <i class="bi bi-three-dots-vertical"></i>
                            </div>
                            <div class="col-sm " style="height: 95%;">
                                <img src="{{ asset('storage/ulasan/' . $u->profil_user) }}" class="rounded-circle"
                                    alt="user" style="height:50px;  object-fit: cover" />
                                <span>{{ $u->username }}</span><br>
                                <img src="{{ asset('storage/ulasan/' . $u->gambar_ulasan) }}" class="rounded shadow"
                                    alt="ulasan" style="height:50px;  object-fit: cover;" />
                                <p>{{ $u->ulasan }}</p>
                                <i class="bi bi-hand-thumbs-up-fill"> </i>
                                <span>Membantu</span>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col text-end">
                <div class="dropdown ">
                    <span><b>Urutkan</b></span>
                    <button class="btn btn-default dropdown-toggle ms-3 " type="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        Terbaru
                    </button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Terbaru</a></li>
                        <li><a class="dropdown-item" href="#">Rating Tertinggi</a></li>
                        <li><a class="dropdown-item" href="#">Rating Terrendah</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- <div class="col-sm-2 mb-3 mt-0">
                                                                                                                                                                                                                                                                <i class="bi bi-star-fill text-warning" style="font-size: 15px"></i>
                                                                                                                                                                                                                                                                <span class="puas">5</span>
                                                                                                                                                                                                                                                                <span class="puas">
                                                                                                                                                                                                                                                                    <div class="progress" role="progressbar" aria-label="Warning example" aria-valuenow="75" aria-valuemin="0"
                                                                                                                                                                                                                                                                        aria-valuemax="100">
                                                                                                                                                                                                                                                                        <div class="progress-bar bg-warning" style="width: 75%"></div>
                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                </span>
                                                                                                                                                                                                                                                            </div> -->


    </div>
@endsection
