@extends('toko.toko')

<style>
    img {
        border-radius: 15px;
    }

    #angka {
        opacity: 0;
        visibility: hidden;
        margin: 0;
    }
</style>
@section('konten')
    <div class="container">
        <div class="row">
            @for ($i = 0; $i <= 1; $i++)
                <div class="beranda-content" style="margin: 0;">
                    <p id="angka">{{ $i }}</p>
                    <img src="./imgs/1662slide2.jpg" class="border border-dark-subtle" alt="gambar1" width=1000>
                    <br><br>
                    <img src="./imgs/3657slide3.jpg" class="border border-dark-subtle" alt="gambar1" width=1000>
                </div>
            @endfor
        </div>

        <!--card produk-->
        <div class="col justify-content-start mb-3">
            <div class="row">
                <!--card produk-->
                <span><b>Semua Produk</b></span>
                @for ($i = 0; $i <= 9; $i++)
                    @foreach ($produk as $p)
                        <div class="col-sm-3 mb-3">
                            <p id="angka">{{ $i }}</p>
                            <div class="card" style="height: 95%; width: 100%;">
                                <img src="{{ asset('storage/product/' . $p->gambar_product) }}" class="card-img-top"
                                    alt="produk" style="width: 100%; height: 150px; object-fit: cover;" />
                                <div class="card-body">
                                    <p class="card-text">{{ $p->nama_product }}</p>
                                    <b>
                                        <p class="card-text"><span>Rp. </span>{{ number_format($p->harga_product) }}</p>
                                    </b>
                                </div>
                                <i class="bi bi-three-dots text-end me-2"></i>
                            </div>
                        </div>
                    @endforeach
                @endfor
                <!--Akhir card produk-->

                <!--pagination-->
                <div class="daftar-produk mt-4">
                    <ul class="pagination">
                        <p>Jumlah Produk Per halaman</p>
                        <span class="page-item ms-4 me-2">
                            <li class="page-item">
                                <a class="page-link" href="#">
                                    <center>
                                        20
                                    </center>
                                </a>
                            </li>
                        </span>
                        <span class="page-item me-2">
                            <li class="page-item">
                                <a class="page-link" href="#">
                                    <center>
                                        40
                                    </center>
                                </a>
                            </li>
                        </span>
                        <span class="page-item">
                            <li class="page-item">
                                <a class="page-link" href="#">
                                    <center>
                                        80
                                    </center>
                                </a>
                            </li>
                        </span>
                    </ul>
                    <ul class="pagination  justify-content-end ">
                        <span>
                            <li class="page-item">
                                <a class="page-link" href="#">
                                    <i class="bi bi-chevron-right"></i>
                                </a>
                            </li>
                        </span>
                    </ul>
                </div>

                <!--akhir pagination-->
            </div>
        </div>

    </div>
@endsection
