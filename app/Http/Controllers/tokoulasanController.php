<?php

namespace App\Http\Controllers;

use App\Models\ulasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class tokoulasanController extends Controller
{
    public function index()
    {
        $ulasan = ulasan::all();
        return view ('ulasan',[
            'ulasan' =>$ulasan,
        ]);
    }
}
