<?php

namespace App\Http\Controllers;

use App\Models\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class tokoProdukController extends Controller
{
    
    public function index()
    {
        $produk = product::all();
        return view ('product',[
            'produk' =>$produk,
        ]);
    }
    public function beranda()
    {
        $produk = product::all();
        return view ('beranda',[
            'produk' =>$produk,
        ]);
    }
}
