<?php

namespace App\Http\Controllers;

use App\Models\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class productController extends Controller
{
    public function index()
    {
        $produk= product::latest()->paginate(10);
        return view ('produk.index', compact('produk'));
    }

    public function create()
    {
        return view ('produk.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_product'  => 'required',
            'harga_product' => 'required|numeric',
            'gambar_product'=> 'required|image|mimes:png,jpg,jpeg',
        ]);

        $gambar_product = $request->file('gambar_product');
        $gambar_product->storeAs('public/product', $gambar_product->hashName());

        $p = product::create([
            'nama_product'  => $request->nama_product,
            'harga_product' => $request->harga_product,
            'gambar_product'=> $gambar_product->hashName()
        ]);
        if ($p){
            return redirect()->route('produk.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            return redirect()->route('produk.index')->with(['error' => 'Data gagal Disimpan!']);
        }
    }


    public function edit(product $produk){
        return view ('produk.edit', compact('product'));
    }

    public function update(Request $request, product $produk){
        $this->validate($request, [
            'nama_product'  => 'required',
            'harga_product' => 'required|numeric',
            'gambar_product'=> 'required|image|mimes:png,jpg,jpeg',
        ]);

       $produk= product::findOrFail($produk->id);

        if ($request->file('gambar_product') == "") {

            $produk->update([
            'nama_product'  => $request->nama_product,
            'harga_product' => $request->harga_product,
            ]);
        }else{
            Storage:: disk('local')->delete('public/product'.$produk->gambar_product);

            $gambar_product = $request->file('gambar_product');
            $gambar_product->storeAs('public/product', $gambar_product->hashName());
            
            $produk->update([
                'nama_product'  => $request->nama_product,
                'harga_product' => $request->harga_product,
                'gambar_product'=> $gambar_product->hashName()
            ]);
        }
        if ($produk){
            return redirect()->route('produk.index')->with(['success' => 'Data Berhasil Diupdate!']);
        }else{
            return redirect()->route('produk.index')->with(['error' => 'Data gagal Diupdate!']);
        }
    }

    public function destroy($id)
    {
       $produk= product::findOrFail($id);
        Storage::disk('local')->delete('public/product'.$produk->gambar_product);
        $produk->delete();
        
        if ($produk){
            return redirect()->route('produk.index')->with(['success' => 'Data Berhasil Dihapus!']);
        }else{
            return redirect()->route('produk.index')->with(['error' => 'Data gagal Dihapus!']);
        }
    }
}
