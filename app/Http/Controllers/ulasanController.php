<?php

namespace App\Http\Controllers;

use App\Models\ulasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ulasanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ulasan= ulasan::latest()->paginate(10);
        return view ('ulasan.index', compact('ulasan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('ulasan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'profil_user'  => 'required|image|mimes:png,jpg,jpeg,jfif',
            'username' => 'required',
            'gambar_ulasan'=> 'required|image|mimes:png,jpg,jpeg,jfif',
            'ulasan'=> 'required'
        ]);

        $profil_user = $request->file('profil_user');
        $profil_user->storeAs('public/ulasan', $profil_user->hashName());
        $gambar_ulasan = $request->file('gambar_ulasan');
        $gambar_ulasan->storeAs('public/ulasan', $gambar_ulasan->hashName());

        $ulasan = ulasan::create([
            'profil_user'=> $profil_user->hashName(),
            'username'  => $request->username,
            'gambar_ulasan'=> $gambar_ulasan->hashName(),
            'ulasan' => $request->ulasan
        ]);
        if ($ulasan){
            return redirect()->route('ulasan.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            return redirect()->route('ulasan.index')->with(['error' => 'Data gagal Disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ulasan $ulasan)
    {
        return view ('ulasan.edit', compact('ulasan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ulasan $ulasan)
    {
        $this->validate($request, [
            'profil_user'  => 'required|image|mimes:png,jpg,jpeg,jfif',
            'username' => 'required',
            'gambar_ulasan'=> 'required|image|mimes:png,jpg,jpeg,jfif',
            'ulasan'=> 'required'
        ]);

       $ulasan= ulasan::findOrFail($ulasan->id);

        if ($request->file('profil_user') == "") {

            $produk->update([
            'username'  => $request->username,
            'ulasan' => $request->ulasan,
            ]);
        }else if($request->file('gambar_ulasan') == ""){
            $produk->update([
                'username'  => $request->username,
                'ulasan' => $request->ulasan,
                ]);
        }else{
            Storage:: disk('local')->delete('public/ulasan'.$produk->profil_user);
            Storage:: disk('local')->delete('public/ulasan'.$produk->gambar_ulasan);

            $profil_user = $request->file('profil_user');
            $profil_user->storeAs('public/ulasan', $profil_user->hashName());
            $gambar_ulasan = $request->file('gambar_ulasan');
            $gambar_ulasan->storeAs('public/ulasan', $gambar_ulasan->hashName());
            
            $ulasan->update([
            'profil_user'=> $profil_user->hashName(),
            'username'  => $request->username,
            'gambar_ulasan'=> $gambar_ulasan->hashName(),
            'ulasan' => $request->ulasan
            ]);
        }
        if ($produk){
            return redirect()->route('ulasan.index')->with(['success' => 'Data Berhasil Diupdate!']);
        }else{
            return redirect()->route('ulasan.index')->with(['error' => 'Data gagal Diupdate!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ulasan= ulasan::findOrFail($id);
        Storage::disk('local')->delete('public/ulasan'.$ulasan->gambar_product);
        $ulasan->delete();
        
        if ($ulasan){
            return redirect()->route('ulasan.index')->with(['success' => 'Data Berhasil Dihapus!']);
        }else{
            return redirect()->route('ulasan.index')->with(['error' => 'Data gagal Dihapus!']);
        }
    }
}
