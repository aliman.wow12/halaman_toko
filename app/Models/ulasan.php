<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ulasan extends Model
{
    use HasFactory;

    protected $fillable = [
        'profil_user', 'username', 'gambar_ulasan', 'ulasan'
    ];
    public $table = "ulasan";
}
