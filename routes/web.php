<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
// use App\Http\Controllers\produkController;
use App\Http\Controllers\productController;
use App\Http\Controllers\tokoProdukController;
use App\Http\Controllers\ulasanController;
use App\Http\Controllers\tokoulasanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// toko
Route::get('/', fn () => redirect ()->route('beranda'));
// Route::get('/', [tokoProdukController::class, 'index'])->name('');
// Route::get('/', function () {
//     return view('beranda');
// });
// Route::get('/beranda', function () {
//     return view('beranda');
// });
Route::get('/beranda', [tokoProdukController::class, 'beranda'])->name('beranda');
Route::get('/product', [tokoProdukController::class, 'index'])->name('product');
Route::get('/Ulasan', [tokoulasanController::class, 'index'])->name('ulasan');
// Route::get('/product', function () {
// return view('product');
// });
// Route::resource('/product', productController::class);
Route::get('/ulasan', function () {
    return view('ulasan');
});

// admin
Route::get('/admin', fn () => redirect ()->route('login'));

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('home');
    })->name('dashboard');
});
Route::group(['middleware'=> 'auth'], function (){
//     // Route::get("/product/data", [productController::class, 'data'])->name('product.data');
    Route::get('/produk', function(){
        return view('produk.index');
    });
    Route::resource('/produk', productController::class);
    Route::resource('/ulasan', ulasanController::class);
});
// Route::resource('produk', produkController::class);
// Route::group(['middleware'=>'auth'], function(){
//     Route::get('/produk', [productController::class, 'index'])->name('produk');
//     Route::get('/produk/create', [productController::class, 'create'])->name('produk.create');
//     Route::post('/produk/store', [productController::class, 'store'])->name('produk.store');
//     Route::get('/produk/edit/', [productController::class, 'edit'])->name('produk.update');
//     // Route::put('/produk/update/',[productController::class, 'update'])->name('produk.update');
//     Route::delete('/produk/destroy/',[ productController::class, 'destroy'])->name('produk.destroy');
// });
